/* SNEK by Kresimir, 2022.

This file contains the entire source code for the game SNEK.

Compile it on any POSIX system using a C11 compiler (GCC and Clang tested):

  cc snek.c -o snek -Wall -O2

*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <termios.h>
#include <getopt.h>

extern int errno;

// terminal handling & interrupts
#define ENCODING_UTF_8

#ifdef ENCODING_UTF_8 // block characters in UTF-8 (change for other encodings)
  #define TOP_BLOCK    "\u2580"
  #define BOTTOM_BLOCK "\u2584"
  #define FULL_BLOCK   "\u2588"
#endif

#define HIDE_CURSOR_ES "\e[?25l"
#define SHOW_CURSOR_ES "\e[?25h"

static struct termios g_remember_tty_state;
static bool check_terminal_size(const size_t lines, const size_t cols);
static bool check_truecolor();
static void set_raw_mode(void);
static void cleanup(void);
static void signal_handler(int signum);

static void parse_args(int argc, char** argv);
static void print_help(FILE *stream);

// input handling
static int kbhit();

enum action {
  a_NOTHING,
  a_UP,
  a_DOWN,
  a_LEFT,
  a_RIGHT,
  a_PAUSE,
  a_QUIT
};

static enum action get_key(void);

#define INPUT_QUEUE_CAPACITY 5
static size_t g_input_queue_front = SIZE_MAX;
static size_t g_input_queue_back = SIZE_MAX;
static enum action g_input_queue[INPUT_QUEUE_CAPACITY];
static enum action input_queue_pop(void);
static void input_queue_push(enum action a);

// SPEED SETTINGS
#define TICKS_PER_FRAME 10
struct timespec g_splash_duration   = { 1, 0 };         // { sec, nsec }
struct timespec g_gameover_duration = { 0, 500000000 }; // { sec, nsec }
#define TICK_DURATION_K -1125000l
#define TICK_DURATION_L 13125000l
#define ARG_SPEED_DEFAULT 5
  // g_tick_duration = { 0, TICK_DURATION_K * arg_speed + TICK_DURATION_L };
struct timespec g_tick_duration;

// GAME SIZE
#define COLS_MIN 18
#define ROWS_MIN 14

#define COLS_DEFAULT 77
#define ROWS_DEFAULT 44

#define COLS_MAX 160
#define ROWS_MAX 100

// global variables
static bool g_exit_main_loop = false;
static bool g_repeat_game    = false;
static bool g_game_paused    = false;

enum cell_type {
  EMPTY_CELL = 0,
  MAX_LEN = 2000,
  WALL_CELL = 70000,
  FOOD_CELL = 70001
};
static enum cell_type g_grid[ROWS_MAX * COLS_MAX];
static enum cell_type* cell(size_t r, size_t c);

static size_t g_grid_height, g_grid_width;
static size_t g_head_r, g_head_c;  // coordinates of Snek's head.
static unsigned g_length;          // length of Snek.

enum direction {
  UP,
  DOWN,
  LEFT,
  RIGHT
};
static enum direction g_direction; // direction of Snek

// game logic
#define STARTING_SIZE  6
#define SIZE_INCREMENT 3

static void setup_game(const size_t start_length);
static void show_splash(void);
static void move_cursor_back(const size_t lines);
static void print_status(void);
static void display_grid(void);

static bool g_truecolor;
static void print_pixel_pair_monoch(enum cell_type top, enum cell_type bottom);
static void print_pixel_pair_colour(enum cell_type top, enum cell_type bottom);
static void (*print_pixel_pair)(enum cell_type, enum cell_type) = &print_pixel_pair_monoch;

static void move_snek(void);
static void spawn_food(void);

static float lerp(float first, float second, float t);
static unsigned min(unsigned a, unsigned b);


// MAIN //////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {
  parse_args(argc, argv);

  // each terminal line contains two grid rows
  const size_t n_lines = g_grid_height / 2 + g_grid_height % 2 + 1;

  if(!check_terminal_size(n_lines + 1, g_grid_width + 1)) {
    exit(EXIT_FAILURE);
  }

  if (g_truecolor) {
    print_pixel_pair = &print_pixel_pair_colour;
  }

  atexit(cleanup);
  signal(SIGINT, signal_handler);
  set_raw_mode();
  show_splash();

  do {
    time_t current_time;
    srand((unsigned) time(&current_time));

    setup_game(STARTING_SIZE);
    nanosleep(&g_splash_duration, NULL);

    unsigned tick = 0;

    while (!g_exit_main_loop) {
      if (!(tick %= TICKS_PER_FRAME) && !g_game_paused) {

        enum action a = input_queue_pop();
        if (a == a_UP    && g_direction != DOWN ) g_direction = UP;
        if (a == a_DOWN  && g_direction != UP   ) g_direction = DOWN;
        if (a == a_LEFT  && g_direction != RIGHT) g_direction = LEFT;
        if (a == a_RIGHT && g_direction != LEFT ) g_direction = RIGHT;

        move_snek();

        move_cursor_back(n_lines);
        print_status();
        display_grid();
      }

      int kb_event = kbhit();
      if (kb_event) {
        enum action a = get_key();
        if (a == a_QUIT) {
          g_exit_main_loop = true;
          g_repeat_game = false;
        }
        else if (a == a_PAUSE) {
          g_game_paused = !g_game_paused;
        }
        else if (a != a_NOTHING) {
          g_game_paused = false;
          input_queue_push(a);
        }
      }
      tick++;

      nanosleep(&g_tick_duration, NULL);
    }
    printf("Game over!");
    fflush(stdout);

    nanosleep(&g_gameover_duration, NULL);
    if (g_repeat_game) {
      printf("\r          "); // clear game over message
      g_exit_main_loop = false;
    }
  } while (g_repeat_game);


  printf("\n");
  return EXIT_SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
enum cell_type* cell(size_t r, size_t c) {
  // returns pointer to the cell in the grid, given row and column
  return &g_grid[r * g_grid_width + c];
}

unsigned min(unsigned a, unsigned b) {
  return (a < b) ? a : b;
}

void setup_game(const size_t start_length) {
  memset(g_grid, 0, g_grid_height * g_grid_width * sizeof(unsigned));
  // make a box
  for (size_t r = 0; r < g_grid_height; ++r) {
    *cell(r, 0)                 = WALL_CELL;
    *cell(r, g_grid_width - 1)  = WALL_CELL;
  }
  for (size_t c = 0; c < g_grid_width; ++c) {
    *cell(0, c)                 = WALL_CELL;
    *cell(g_grid_height - 1, c) = WALL_CELL;
  }

  g_head_r = g_grid_height / 2 + g_grid_height % 2;
  g_head_c = g_grid_width  / 2 + g_grid_width  % 2;

  *cell(g_head_r, g_head_c) = g_length = start_length;
  g_direction = RIGHT;

  spawn_food();
}

void move_snek(void) {
  for (size_t r = 0; r < g_grid_height; ++r) {
    for (size_t c = 0; c < g_grid_width; ++c) {
      if (*cell(r, c) <= MAX_LEN && *cell(r, c) > 0) {
        (*cell(r, c))--;
      }
    }
  }

  if      (g_direction == UP   ) g_head_r = min(g_head_r - 1, g_grid_height);
  else if (g_direction == DOWN ) g_head_r = (g_head_r + 1) % g_grid_height;
  else if (g_direction == LEFT ) g_head_c = min(g_head_c - 1, g_grid_width);
  else if (g_direction == RIGHT) g_head_c = (g_head_c + 1) % g_grid_width;
  else {
    fprintf(stderr, "ERROR: unreachable branch in move_snek\n");
    exit(EXIT_FAILURE);
  }

  // check for food and game over
  if (*cell(g_head_r, g_head_c) == FOOD_CELL) {
    if (g_length + SIZE_INCREMENT <= MAX_LEN) {
      g_length += SIZE_INCREMENT;
      for (size_t r = 0; r < g_grid_height; ++r) {
        for (size_t c = 0; c < g_grid_width; ++c) {
          if (*cell(r, c) <= MAX_LEN && *cell(r, c) > 0) {
            (*cell(r, c)) += SIZE_INCREMENT;
          }
        }
      }
    }
    spawn_food();
  }
  else if (*cell(g_head_r, g_head_c) != EMPTY_CELL) {
    g_exit_main_loop = true;
  }

  *cell(g_head_r, g_head_c) = g_length;
}

void spawn_food(void) {
  while(1) {
    size_t r = rand() % g_grid_height;
    size_t c = rand() % g_grid_width;

    if (*cell(r, c) == EMPTY_CELL) {
      *cell(r, c) = FOOD_CELL;
      return;
    }
  }
}

void move_cursor_back(size_t lines) {
  printf("\r\033[%zuA", lines);
}

void print_status(void) { // TODO more info perhaps? probably not...
  printf("Length: %u     \n", g_length);
}

void display_grid(void) {
  for (size_t r = 0; r < g_grid_height - 1; r += 2) {
    for (size_t c = 0; c < g_grid_width; ++c) {
      print_pixel_pair(*cell(r, c), *cell(r + 1, c));
    }
    printf("\n");
  }
  if (g_grid_height % 2) { // for odd height, print last row
    for (size_t c = 0; c < g_grid_width; ++c) {
      print_pixel_pair(*cell(g_grid_height - 1, c), EMPTY_CELL);
    }
    printf("\n");
  }
  fflush(stdout);
}

float lerp(float l, float h, float t) {
  if (t <= 0) return l;
  if (t >= 1) return h;
  return l + t * (h - l);
}

void print_pixel_pair_colour(enum cell_type top, enum cell_type bottom) {
  // colour escape sequences
  static const char* colour_clear = "\e[0m";
  static const char* colour_fg = "\e[38;2;";
  static const char* colour_bg = "\e[48;2;";

  static unsigned char red[]   = { 0, 0 };
  static unsigned char green[] = { 0, 0 };
  static unsigned char blue[]  = { 0, 0 };

  {
    const enum cell_type pixel_type[] = { top, bottom };
    for (size_t i = 0; i < 2; i++) { // do twice, for top and bottom
      // Snek
      if (pixel_type[i] > 0 && pixel_type[i] <= MAX_LEN) {
        const float stripe = ((pixel_type[i] + 2) % 3) ? 1. : 0.4;
        red[i]   = lerp(128, 255, stripe * pixel_type[i] / g_length);
        green[i] = lerp(100, 220, stripe * pixel_type[i] / g_length);
        blue[i]  = 0;
      }
      // Walls
      else if (pixel_type[i] == WALL_CELL) {
        red[i]   = 92;
        green[i] = 92;
        blue[i]  = 92;
      }
      // Food
      else if (pixel_type[i] == FOOD_CELL) {
        red[i]   = 255;
        green[i] = 92;
        blue[i]  = 48;
      }
    }
  }
  // draw
  if (top == EMPTY_CELL && bottom == EMPTY_CELL) {
    printf(" ");
    return;
  }

  if (top == EMPTY_CELL) {
    printf("%s%u;%u;%um" BOTTOM_BLOCK "%s", colour_fg, red[1], green[1], blue[1], colour_clear);
    return;
  }

  if (bottom == EMPTY_CELL) {
    printf("%s%u;%u;%um" TOP_BLOCK "%s", colour_fg, red[0], green[0], blue[0], colour_clear);
    return;
  }

  if (bottom == top) {
    printf("%s%u;%u;%um" FULL_BLOCK "%s", colour_fg, red[0], green[0], blue[0], colour_clear);
    return;
  }

  printf("%s%u;%u;%um%s%u;%u;%um" TOP_BLOCK "%s",
         colour_fg,
         red[0], green[0], blue[0],
         colour_bg,
         red[1], green[1], blue[1],
         colour_clear);
}

void print_pixel_pair_monoch(enum cell_type top, enum cell_type bottom) {
  if (top == EMPTY_CELL && bottom == EMPTY_CELL) {
    printf(" ");
  }
  else if (top == EMPTY_CELL && bottom != EMPTY_CELL) {
    printf(BOTTOM_BLOCK);
  }
  else if (top != EMPTY_CELL && bottom == EMPTY_CELL) {
    printf(TOP_BLOCK);
  }
  else if (top != EMPTY_CELL && bottom != EMPTY_CELL) {
    printf(FULL_BLOCK);
  }
}




// input handling ------------------------------------------------------------
int kbhit() {
  // detect key press
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}

enum action get_key(void) {
  #define key(c0, c1, c2) ((input[0] == c0) \
                        && (input[1] == c1) \
                        && (input[2] == c2))
  char input[6] = { 0, 0, 0, 0, 0, 0 };
  if (read(STDIN_FILENO, &input, 3) == -1) {
    fprintf(stderr, "ERROR reading from stdin: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  /* DEBUG:
  for (size_t i = 0; i < 6; ++i) {
    printf("    %3d ", input[i]);
  }
  //*/

  // Key bindings:
  //  Arrow keys
  if (key(033, '[', 'A')) return a_UP;    // Up arrow
  if (key(033, '[', 'B')) return a_DOWN;  // Down arrow
  if (key(033, '[', 'C')) return a_RIGHT; // Down arrow
  if (key(033, '[', 'D')) return a_LEFT;  // Left arrow
  //  Gamer keys
  if (key('w',   0,   0)) return a_UP;    // w
  if (key('a',   0,   0)) return a_LEFT;  // a
  if (key('s',   0,   0)) return a_DOWN;  // s
  if (key('d',   0,   0)) return a_RIGHT; // d
  //  Gamer keys with CAPS LOCK
  if (key('W',   0,   0)) return a_UP;    // W
  if (key('A',   0,   0)) return a_LEFT;  // A
  if (key('S',   0,   0)) return a_DOWN;  // S
  if (key('D',   0,   0)) return a_RIGHT; // D
  //  Vim keys (ADM-3A)
  if (key('h',   0,   0)) return a_LEFT;  // h
  if (key('j',   0,   0)) return a_DOWN;  // j
  if (key('k',   0,   0)) return a_UP;    // k
  if (key('l',   0,   0)) return a_RIGHT; // l
  //  Vim keys with CAPS LOCK
  if (key('H',   0,   0)) return a_LEFT;  // H
  if (key('J',   0,   0)) return a_DOWN;  // J
  if (key('K',   0,   0)) return a_UP;    // K
  if (key('L',   0,   0)) return a_RIGHT; // L
  //  NumPad with NUM LOCK
  if (key('2',   0,   0)) return a_DOWN;  // 2
  if (key('4',   0,   0)) return a_LEFT;  // 4
  if (key('6',   0,   0)) return a_RIGHT; // 6
  if (key('8',   0,   0)) return a_UP;    // 8
  // Pause
  if (key(' ',   0,   0)) return a_PAUSE; // Spacebar
  if (key('p',   0,   0)) return a_PAUSE; // p
  if (key('P',   0,   0)) return a_PAUSE; // P
  //  Quit
  if (key(033,   0,   0)) return a_QUIT;  // Esc

  return a_NOTHING;
  #undef key
}

void input_queue_push(enum action a) {
  if ( (g_input_queue_front == 0 && g_input_queue_back == INPUT_QUEUE_CAPACITY-1)
    || ( g_input_queue_back == (g_input_queue_front - 1) % (INPUT_QUEUE_CAPACITY - 1)) )
  {
    // queue is full: do nothing
    return;
  }

  if (g_input_queue_front == SIZE_MAX) {
    // queue is empty: insert first element
    g_input_queue_front = g_input_queue_back = 0;
    g_input_queue[g_input_queue_back] = a;
    return;
  }

  if (g_input_queue[g_input_queue_back] == a) {
    // if the same action is already at the back of the queue, do nothing
    return;
  }
  // otherwise
  g_input_queue_back = (g_input_queue_back + 1) % INPUT_QUEUE_CAPACITY;
  g_input_queue[g_input_queue_back] = a;
}

enum action input_queue_pop(void) {
  if (g_input_queue_front == SIZE_MAX) {
    // queue is empty: return a_NOTHING
    return a_NOTHING;
  }

  enum action a = g_input_queue[g_input_queue_front];

  if (g_input_queue_front == g_input_queue_back) {
    g_input_queue_front = g_input_queue_back = SIZE_MAX;
  }
  else {
    g_input_queue_front = (g_input_queue_front + 1) % INPUT_QUEUE_CAPACITY;
  }

  return a;
}

// terminal handling ---------------------------------------------------------
void signal_handler(int signum) {
  g_exit_main_loop = true;
  g_repeat_game = false;
}
void cleanup(void) {
  char ch;
  while ((ch = fgetc(stdin)) != EOF) {
    ; // empty stdin
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &g_remember_tty_state);
  printf(SHOW_CURSOR_ES); // unhide cursor
}

bool check_terminal_size(const size_t lines, const size_t cols) {
  struct winsize term_size;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &term_size) == -1) {
    fprintf(stderr, "ERROR checking TTY dimensions: %s\n", strerror(errno));
    return false;
  }

  if (term_size.ws_col < cols || term_size.ws_row < lines) {
    fprintf(stderr,   "ERROR: Your terminal is too small to fit snek.\n");
    if (term_size.ws_col < cols) {
      fprintf(stderr, "       Needs at least %lu columns, has %d.\n",
              cols,
              term_size.ws_col
              );
    }
    if (term_size.ws_row < lines) {
      fprintf(stderr, "       Needs at least %lu lines, has %d.\n",
              lines,
              term_size.ws_row
              );
    }
    return false;
  }
  return true;
}

static bool check_truecolor() {
  const char* env_colorterm = getenv("COLORTERM");

  if (env_colorterm && strcmp(env_colorterm, "truecolor") == 0) {
    return true;
  }


  return false;
}

void set_raw_mode(void) {
  struct termios tty_state;
  tcgetattr(STDIN_FILENO, &tty_state);

  g_remember_tty_state = tty_state;

  // disable echo, canonical mode (rtfm: man 3 termios)
  tty_state.c_lflag &= ~(ECHO | ICANON /*| ISIG | IEXTEN*/);
  tty_state.c_cc[VMIN] = 0;
  tty_state.c_cc[VTIME] = 0;
  tcsetattr(STDIN_FILENO, TCSANOW, &tty_state);

  printf(HIDE_CURSOR_ES);
}

void parse_args(int argc, char** argv) {
  opterr = 0;
  const struct option long_opts[] = {
    /*NAME         ARGUMENT           FLAG  VAL */
    {"repeat",     no_argument,       NULL, 'r'},
    {"monochrome", no_argument,       NULL, 's'},
    {"width",      required_argument, NULL, 'w'},
    {"height",     required_argument, NULL, 'h'},
    {"speed",      required_argument, NULL, 's'},
    {NULL,         0,                 NULL,  0 },
  };
  const char* short_opts = "rmw:h:s:";

  unsigned arg_width  = COLS_DEFAULT;
  unsigned arg_height = ROWS_DEFAULT;
  g_truecolor = check_truecolor();

  int  arg_speed = ARG_SPEED_DEFAULT;
  bool arg_error = false;

  int c;
  while ((c = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1) {
    char* end_ptr;
    unsigned long value = 0;

    switch (c) {
      case 'r':
        g_repeat_game = true;
        break;
      case 'm':
        g_truecolor = false;
        break;
      case 'w':
        value = strtoul(optarg, &end_ptr, 10);
        if (*end_ptr != '\0' || value < COLS_MIN || value > COLS_MAX) {
          fprintf(stderr, "ERROR: Width must be between %d and %d.\n\n", COLS_MIN, COLS_MAX);
          arg_error = true;
        }
        arg_width = value;
        break;
      case 'h':
        value = strtoul(optarg, &end_ptr, 10);
        if (*end_ptr != '\0' || value < ROWS_MIN || value > ROWS_MAX) {
          fprintf(stderr, "ERROR: Height must be between %d and %d.\n\n", ROWS_MIN, ROWS_MAX);
          arg_error = true;
        }
        arg_height = value;
        break;
      case 's':
        value = strtoul(optarg, &end_ptr, 10);
        if (*end_ptr != '\0' || value < 1 || value > 9) {
          fprintf(stderr, "ERROR: Speed must be between 1 and 9.\n\n");
          arg_error = true;
        }
        arg_speed = value;
        break;
      case 0:
      default:
        arg_error = true;
        break;
    }

    if (arg_error) {
      print_help(stdout);
      exit(EXIT_FAILURE);
    }
  }

  g_tick_duration.tv_sec  = 0;
  g_tick_duration.tv_nsec = TICK_DURATION_K * arg_speed + TICK_DURATION_L; // 13125000l - arg_speed * 1125000l;
  g_grid_width  = arg_width;
  g_grid_height = arg_height;
}

void print_help(FILE *stream) {
  const char* helptext =
    "SNEK - a simple game for the terminal\n"
    "by Kre\u0161imir, 2022\n"
    "\n"
    "Change direction with the arrow keys, WASD, or HJKL.\n"
    "Eat food, grow big.\n"
    "Don't run into your tail or the wall or it's game over.\n"
    "\n"
    "Command line options\n"
    "\n"
    "  --repeat      -r    Restart the game automatically upon game over\n"
    "                      (hint: to exit, press ESC)\n"
    "  --width  N    -w N  Set width  (min 18, max: 160, default: 77)\n"
    "  --height N    -h N  Set height (min 14, max: 100, default: 44)\n"
    "  --speed  N    -s N  Set speed  (min: 1, max:   9, default:  5)\n"
    "  --monochrome  -m    Disable fancy colours. If the environment variable\n"
    "                      COLORTERM is not set to \"truecolor\", this is the\n"
    "                      default behaviour.\n";
  fprintf(stream, "%s", helptext);
}

void show_splash(void) {
  const size_t n_lines = g_grid_height / 2 + g_grid_height % 2 + 1;
  const size_t splash_w = 18;
  const size_t splash_h = 6;
  const size_t splash_colour_w = 41;
  const size_t splash_colour_h = 21;

  if (!g_truecolor || g_grid_width < splash_colour_w || g_grid_height < splash_colour_h) {

    const size_t padding_w = (g_grid_width - splash_w)/2;
    const size_t padding_h = (n_lines - splash_h)/2;

    const char* splash[] = {
      "       " FULL_BLOCK TOP_BLOCK "      \n",
      "       " TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK FULL_BLOCK "   \n",
      "   " TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK TOP_BLOCK "   \n",
      FULL_BLOCK TOP_BLOCK TOP_BLOCK " " FULL_BLOCK BOTTOM_BLOCK " " FULL_BLOCK " " FULL_BLOCK TOP_BLOCK TOP_BLOCK " " FULL_BLOCK " " BOTTOM_BLOCK TOP_BLOCK "\n",
      TOP_BLOCK TOP_BLOCK FULL_BLOCK " " FULL_BLOCK " " TOP_BLOCK FULL_BLOCK " " FULL_BLOCK TOP_BLOCK "  " FULL_BLOCK TOP_BLOCK BOTTOM_BLOCK " \n",
      TOP_BLOCK TOP_BLOCK TOP_BLOCK " " TOP_BLOCK "  " TOP_BLOCK " " TOP_BLOCK TOP_BLOCK TOP_BLOCK " " TOP_BLOCK "  " TOP_BLOCK "\n",

    };
    for (size_t i = 0; i < n_lines; ++i) {
      if (i >= padding_h && i < padding_h + splash_h)
        printf("%*c%s", (unsigned)padding_w, ' ', splash[i - padding_h]);
      else
        printf("\n");
    }

  }
  else {
    const size_t padding_colour_w = (g_grid_width - splash_colour_w)/2;
    const size_t padding_colour_h = (n_lines - splash_colour_h)/2;

    /* splash image generated using fay */
    const char* splash_colour[] = {
      "  \e[48;2;66;66;66m\e[38;2;1;1;1m" TOP_BLOCK "\e[48;2;252;252;252m\e[38;2;59;59;59m" TOP_BLOCK "\e[48;2;255;255;255m\e[38;2;99;99;99m" TOP_BLOCK "\e[48;2;137;137;137m\e[38;2;5;5;5m" TOP_BLOCK "\e[48;2;91;91;91m\e[38;2;1;1;1m" TOP_BLOCK "\e[48;2;253;253;253m\e[38;2;49;49;49m" TOP_BLOCK "\e[48;2;255;255;255m\e[38;2;81;81;81m" TOP_BLOCK "\e[48;2;171;171;171m\e[38;2;5;5;5m" TOP_BLOCK "\e[0m\e[38;2;1;1;1m" BOTTOM_BLOCK "                             \e[0m\n",
      " \e[38;2;1;1;1m" FULL_BLOCK "\e[48;2;137;148;95m\e[38;2;167;169;159m" TOP_BLOCK "\e[48;2;72;76;56m\e[38;2;156;173;91m" TOP_BLOCK "\e[48;2;146;160;91m\e[38;2;217;223;192m" TOP_BLOCK "\e[48;2;116;116;116m\e[38;2;130;130;130m" TOP_BLOCK "\e[48;2;140;155;78m\e[38;2;221;223;213m" TOP_BLOCK "\e[48;2;110;121;64m\e[38;2;182;195;134m" TOP_BLOCK "\e[48;2;167;182;108m\e[38;2;237;240;225m" TOP_BLOCK "\e[48;2;255;255;255m\e[38;2;254;254;254m" TOP_BLOCK "\e[48;2;84;84;84m\e[38;2;40;40;40m" TOP_BLOCK "\e[0m\e[38;2;40;40;40m                             \e[0m\n",
      " \e[48;2;46;36;1m\e[38;2;1;1;1m" TOP_BLOCK "\e[48;2;85;81;43m\e[38;2;95;103;63m" TOP_BLOCK "\e[48;2;109;118;71m\e[38;2;1;1;1m" TOP_BLOCK "\e[48;2;184;190;158m\e[38;2;102;114;54m" TOP_BLOCK "\e[48;2;56;56;54m\e[38;2;118;118;118m" TOP_BLOCK "\e[48;2;86;95;49m\e[38;2;58;69;16m" TOP_BLOCK "\e[48;2;4;5;1m\e[38;2;28;28;28m" TOP_BLOCK "\e[48;2;146;160;93m\e[38;2;108;124;45m" TOP_BLOCK "\e[48;2;244;244;244m\e[38;2;255;255;255m" TOP_BLOCK "\e[48;2;120;99;14m\e[38;2;106;99;71m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;120;96;1m" TOP_BLOCK "\e[48;2;241;192;1m\e[38;2;19;15;1m" TOP_BLOCK "\e[0m\e[38;2;112;90;1m" BOTTOM_BLOCK "                          \e[0m\n",
      "\e[48;2;110;88;1m\e[38;2;11;9;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;224;179;1m" TOP_BLOCK "\e[38;2;220;176;1m" TOP_BLOCK "\e[38;2;118;97;14m" TOP_BLOCK "\e[38;2;130;105;6m" TOP_BLOCK "\e[38;2;177;142;1m" TOP_BLOCK "\e[48;2;156;125;3m\e[38;2;130;134;113m" TOP_BLOCK "\e[48;2;112;106;79m\e[38;2;171;182;128m" TOP_BLOCK "\e[48;2;107;98;58m\e[38;2;244;245;237m" TOP_BLOCK "\e[48;2;191;153;1m\e[38;2;121;118;110m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;218;174;1m" TOP_BLOCK "   \e[38;2;174;139;1m" TOP_BLOCK "\e[48;2;173;138;1m\e[38;2;7;6;1m" TOP_BLOCK "\e[0m\e[38;2;7;6;1m                        \e[0m\n",
      "\e[48;2;193;155;1m\e[38;2;181;145;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;220;176;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;206;166;1m" TOP_BLOCK "\e[48;2;149;121;1m\e[38;2;73;58;1m" TOP_BLOCK "\e[0m\e[38;2;15;12;1m" BOTTOM_BLOCK "                      \e[0m\n",
      "\e[38;2;118;94;1m" TOP_BLOCK "\e[48;2;168;134;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;252;202;1m" TOP_BLOCK "\e[48;2;203;162;1m" TOP_BLOCK "\e[48;2;87;69;1m" TOP_BLOCK "\e[0m\e[38;2;205;164;1m" TOP_BLOCK "\e[38;2;103;82;1m" TOP_BLOCK "\e[38;2;73;58;1m" TOP_BLOCK "\e[38;2;109;87;1m" TOP_BLOCK "\e[48;2;7;5;1m\e[38;2;213;170;1m" TOP_BLOCK "\e[48;2;179;143;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;229;183;1m" TOP_BLOCK "\e[48;2;180;145;1m\e[38;2;254;203;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;211;169;1m" TOP_BLOCK "\e[38;2;157;127;1m" TOP_BLOCK "  \e[48;2;135;109;1m\e[38;2;81;65;1m" TOP_BLOCK "\e[0m\e[38;2;81;65;1m                      \e[0m\n",
      "  \e[38;2;8;6;1m" TOP_BLOCK "       \e[38;2;15;12;1m" TOP_BLOCK "\e[48;2;107;86;1m\e[38;2;150;121;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;42;34;1m\e[38;2;17;14;1m" TOP_BLOCK "\e[0m\e[38;2;17;14;1m                     \e[0m\n",
      "           \e[48;2;111;89;1m\e[38;2;84;68;1m" TOP_BLOCK "\e[48;2;193;155;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;190;153;1m" TOP_BLOCK "\e[48;2;191;154;1m" TOP_BLOCK "\e[48;2;198;159;1m" TOP_BLOCK "\e[48;2;211;169;1m" TOP_BLOCK "\e[48;2;231;185;1m" TOP_BLOCK "\e[48;2;76;61;1m\e[38;2;52;42;1m" TOP_BLOCK "\e[0m\e[38;2;52;42;1m                     \e[0m\n",
      "          \e[38;2;13;10;1m" BOTTOM_BLOCK "\e[48;2;245;196;1m\e[38;2;181;145;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;252;202;1m" TOP_BLOCK "\e[48;2;13;11;1m\e[38;2;55;44;1m" TOP_BLOCK "\e[0m\e[38;2;55;44;1m                     \e[0m\n",
      "         \e[38;2;8;7;1m" BOTTOM_BLOCK "\e[48;2;176;141;1m\e[38;2;110;88;1m" TOP_BLOCK "\e[48;2;253;202;1m\e[38;2;255;204;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;121;97;1m\e[38;2;203;162;1m" TOP_BLOCK "\e[0m\e[38;2;203;162;1m                      \e[0m\n",
      "        \e[38;2;35;28;1m" BOTTOM_BLOCK "\e[48;2;154;124;1m\e[38;2;88;71;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK "\e[48;2;184;148;1m" BOTTOM_BLOCK "\e[48;2;165;133;1m\e[38;2;248;198;1m" TOP_BLOCK "\e[48;2;213;171;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;249;199;1m" TOP_BLOCK FULL_BLOCK "\e[48;2;161;128;1m\e[38;2;250;200;1m" TOP_BLOCK "\e[0m\e[38;2;27;21;1m" TOP_BLOCK "   \e[38;2;40;32;1m" BOTTOM_BLOCK "\e[38;2;114;92;1m" BOTTOM_BLOCK "\e[48;2;154;124;1m\e[38;2;10;8;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;39;32;1m" TOP_BLOCK "\e[38;2;50;41;1m" TOP_BLOCK "\e[48;2;195;157;1m\e[38;2;65;52;1m" TOP_BLOCK "\e[48;2;254;203;1m\e[38;2;34;27;1m" TOP_BLOCK "\e[0m\e[38;2;214;171;1m" BOTTOM_BLOCK "\e[38;2;98;78;1m" BOTTOM_BLOCK "          \e[0m\n",
      "       \e[48;2;100;81;1m\e[38;2;7;5;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;133;108;1m" TOP_BLOCK "     \e[38;2;158;127;1m" TOP_BLOCK "\e[48;2;86;69;1m\e[38;2;171;138;1m" TOP_BLOCK "\e[0m\e[38;2;31;25;1m" TOP_BLOCK " \e[38;2;4;3;1m" BOTTOM_BLOCK "\e[38;2;112;90;1m" BOTTOM_BLOCK "\e[48;2;156;126;1m\e[38;2;93;75;1m" TOP_BLOCK "\e[38;2;155;125;1m" TOP_BLOCK "    \e[38;2;158;127;1m" TOP_BLOCK "\e[48;2;218;175;1m\e[38;2;247;197;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK "\e[48;2;182;146;1m" BOTTOM_BLOCK "\e[48;2;180;144;1m\e[38;2;12;9;1m" TOP_BLOCK "\e[0m\e[38;2;12;9;1m        \e[0m\n",
      "     \e[38;2;14;11;1m" BOTTOM_BLOCK "\e[48;2;226;181;1m\e[38;2;71;56;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;239;191;1m" TOP_BLOCK "\e[48;2;236;189;1m\e[38;2;163;131;1m" TOP_BLOCK "\e[48;2;167;134;1m\e[38;2;156;126;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;155;125;1m" TOP_BLOCK "\e[48;2;40;32;1m\e[38;2;136;110;1m" TOP_BLOCK "\e[0m\e[38;2;7;6;1m" TOP_BLOCK " \e[38;2;79;63;1m" BOTTOM_BLOCK "\e[48;2;186;149;1m\e[38;2;100;81;1m" TOP_BLOCK "\e[48;2;165;133;1m\e[38;2;156;126;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;214;172;1m\e[38;2;206;165;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;185;148;1m\e[38;2;85;68;1m" TOP_BLOCK "\e[0m\e[38;2;85;68;1m       \e[0m\n",
      "    \e[38;2;42;34;1m" BOTTOM_BLOCK "\e[48;2;251;201;1m\e[38;2;146;117;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;247;198;1m" BOTTOM_BLOCK "\e[48;2;195;157;1m" BOTTOM_BLOCK "\e[48;2;246;196;1m\e[38;2;158;127;1m" TOP_BLOCK "\e[48;2;172;138;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;4;3;1m\e[38;2;87;70;1m" TOP_BLOCK "\e[0m\e[38;2;87;70;1m  \e[48;2;107;86;1m\e[38;2;10;8;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;230;184;1m" TOP_BLOCK " \e[38;2;254;203;1m" TOP_BLOCK "\e[38;2;223;179;1m" TOP_BLOCK "\e[48;2;249;199;1m\e[38;2;169;136;1m" TOP_BLOCK "\e[48;2;184;148;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;107;87;1m" TOP_BLOCK "\e[0m\e[38;2;112;90;1m" TOP_BLOCK "\e[38;2;35;28;1m" TOP_BLOCK "\e[38;2;24;19;1m" TOP_BLOCK "\e[48;2;5;4;1m\e[38;2;146;117;1m" TOP_BLOCK "\e[48;2;252;202;1m\e[38;2;255;204;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;229;183;1m\e[38;2;235;188;1m" TOP_BLOCK "\e[38;2;1;1;1m" FULL_BLOCK "\e[0m\e[38;2;1;1;1m     \e[48;2;20;16;1m" TOP_BLOCK "\e[0m\n",
      "   \e[38;2;21;17;1m" BOTTOM_BLOCK "\e[48;2;238;190;1m\e[38;2;169;135;1m" TOP_BLOCK "\e[38;2;255;204;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;128;102;1m\e[38;2;248;198;1m" TOP_BLOCK "\e[0m\e[38;2;44;35;1m" TOP_BLOCK "  \e[38;2;59;47;1m" BOTTOM_BLOCK "\e[48;2;255;204;1m\e[38;2;212;169;1m" TOP_BLOCK "     \e[48;2;161;129;1m\e[38;2;246;197;1m" TOP_BLOCK "\e[0m\e[38;2;22;17;1m" TOP_BLOCK "   \e[48;2;56;45;1m\e[38;2;13;10;1m" TOP_BLOCK "\e[48;2;185;149;1m\e[38;2;254;203;1m" TOP_BLOCK "\e[48;2;164;132;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;230;184;1m" TOP_BLOCK "\e[38;2;185;149;1m" TOP_BLOCK "\e[48;2;108;87;1m\e[38;2;144;116;1m" TOP_BLOCK "\e[0m\e[38;2;144;116;1m      \e[48;2;84;68;1m\e[38;2;61;49;1m" TOP_BLOCK "\e[0m\n",
      "   \e[48;2;114;92;1m\e[38;2;73;59;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;161;130;1m" TOP_BLOCK "\e[38;2;216;173;1m" TOP_BLOCK "\e[48;2;171;138;1m\e[38;2;253;203;1m" TOP_BLOCK "\e[48;2;209;168;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;234;188;1m" TOP_BLOCK "\e[48;2;245;196;1m" TOP_BLOCK "\e[48;2;103;83;1m\e[38;2;224;179;1m" TOP_BLOCK "\e[0m\e[38;2;8;6;1m" TOP_BLOCK "  \e[38;2;11;9;1m" BOTTOM_BLOCK "\e[48;2;161;130;1m\e[38;2;156;125;1m" TOP_BLOCK "\e[48;2;193;155;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;230;184;1m" TOP_BLOCK "\e[48;2;254;203;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;229;183;1m" TOP_BLOCK "\e[48;2;2;1;1m\e[38;2;60;48;1m" TOP_BLOCK "\e[0m\e[38;2;60;48;1m   \e[38;2;20;16;1m" BOTTOM_BLOCK "\e[48;2;154;124;1m\e[38;2;110;89;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;131;106;1m" TOP_BLOCK "\e[0m\e[38;2;50;40;1m" TOP_BLOCK "     \e[48;2;116;94;1m\e[38;2;47;38;1m" TOP_BLOCK "\e[48;2;40;32;1m\e[38;2;73;59;1m" TOP_BLOCK "\e[0m\n",
      "  \e[38;2;1;1;1m" BOTTOM_BLOCK "\e[48;2;148;120;1m\e[38;2;140;113;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;144;116;1m\e[38;2;155;125;1m" TOP_BLOCK "\e[0m\e[38;2;11;9;1m" TOP_BLOCK "  \e[38;2;28;23;1m" BOTTOM_BLOCK "\e[48;2;150;121;1m\e[38;2;78;63;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;179;144;1m" BOTTOM_BLOCK "\e[48;2;159;128;1m\e[38;2;238;190;1m" TOP_BLOCK "\e[48;2;221;177;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;57;45;1m\e[38;2;147;118;1m" TOP_BLOCK "\e[0m\e[38;2;147;118;1m    \e[48;2;193;155;1m\e[38;2;82;66;1m" TOP_BLOCK "\e[48;2;214;172;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;208;167;1m" TOP_BLOCK "\e[48;2;209;168;1m" TOP_BLOCK "\e[48;2;150;120;1m" TOP_BLOCK "\e[0m\e[38;2;48;39;1m" TOP_BLOCK "     \e[48;2;150;120;1m\e[38;2;37;30;1m" TOP_BLOCK "\e[48;2;108;87;1m\e[38;2;150;121;1m" TOP_BLOCK "\e[0m\e[38;2;5;4;1m" TOP_BLOCK "\e[0m\n",
      "   \e[48;2;119;96;1m\e[38;2;143;115;1m" TOP_BLOCK "\e[38;2;156;126;1m" FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;153;124;1m" BOTTOM_BLOCK "\e[48;2;174;140;1m\e[38;2;15;12;1m" TOP_BLOCK "\e[0m\e[38;2;199;159;1m" BOTTOM_BLOCK "\e[48;2;251;200;1m\e[38;2;41;33;1m" TOP_BLOCK "\e[48;2;254;203;1m\e[38;2;190;153;1m" TOP_BLOCK "\e[48;2;184;148;1m\e[38;2;156;126;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;61;49;1m\e[38;2;132;107;1m" TOP_BLOCK "\e[0m\e[38;2;132;107;1m    \e[48;2;42;34;1m\e[38;2;14;11;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;254;203;1m" TOP_BLOCK "  \e[48;2;199;159;1m" TOP_BLOCK "\e[0m\e[38;2;47;38;1m" TOP_BLOCK "    \e[38;2;28;23;1m" BOTTOM_BLOCK "\e[48;2;237;189;1m\e[38;2;94;75;1m" TOP_BLOCK "\e[48;2;229;183;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;4;3;1m\e[38;2;79;63;1m" TOP_BLOCK "\e[0m\e[38;2;79;63;1m \e[0m\n",
      "   \e[48;2;14;11;1m\e[38;2;75;60;1m" TOP_BLOCK "\e[48;2;149;120;1m\e[38;2;156;126;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;222;178;1m\e[38;2;161;130;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;243;195;1m" TOP_BLOCK "   \e[48;2;253;202;1m\e[38;2;227;182;1m" TOP_BLOCK "\e[48;2;162;130;1m\e[38;2;156;126;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;152;123;1m" TOP_BLOCK "\e[48;2;35;28;1m\e[38;2;131;106;1m" TOP_BLOCK "\e[0m\e[38;2;131;106;1m     \e[48;2;3;2;1m\e[38;2;36;29;1m" TOP_BLOCK "\e[48;2;239;191;1m\e[38;2;255;204;1m" TOP_BLOCK FULL_BLOCK FULL_BLOCK "\e[48;2;216;173;1m\e[38;2;162;130;1m" TOP_BLOCK "\e[0m\e[38;2;6;5;1m" BOTTOM_BLOCK "  \e[38;2;19;16;1m" BOTTOM_BLOCK "\e[48;2;141;114;1m\e[38;2;10;8;1m" TOP_BLOCK "\e[48;2;254;203;1m\e[38;2;201;160;1m" TOP_BLOCK "\e[48;2;226;181;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;11;9;1m\e[38;2;120;96;1m" TOP_BLOCK "\e[0m\e[38;2;120;96;1m  \e[0m\n",
      "    \e[38;2;69;56;1m" TOP_BLOCK "\e[48;2;92;74;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;178;143;1m" TOP_BLOCK "\e[48;2;237;190;1m\e[38;2;159;128;1m" TOP_BLOCK "\e[48;2;255;204;1m\e[38;2;220;176;1m" TOP_BLOCK "      \e[48;2;206;165;1m\e[38;2;185;149;1m" TOP_BLOCK "\e[48;2;148;120;1m\e[38;2;156;126;1m" TOP_BLOCK "\e[48;2;51;41;1m\e[38;2;154;125;1m" TOP_BLOCK "\e[0m\e[38;2;58;47;1m" TOP_BLOCK "       \e[48;2;15;12;1m\e[38;2;144;115;1m" TOP_BLOCK "\e[48;2;213;170;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;254;203;1m" TOP_BLOCK "\e[48;2;206;165;1m" TOP_BLOCK "\e[48;2;156;126;1m\e[38;2;172;138;1m" TOP_BLOCK "\e[38;2;90;73;1m" TOP_BLOCK "\e[38;2;108;87;1m" TOP_BLOCK "\e[38;2;151;122;1m" TOP_BLOCK "\e[38;2;152;123;1m" BOTTOM_BLOCK "\e[48;2;72;57;1m\e[38;2;234;187;1m" TOP_BLOCK "\e[0m\e[38;2;65;52;1m" TOP_BLOCK "   \e[0m\n",
      "      \e[38;2;99;79;1m" TOP_BLOCK "\e[48;2;11;9;1m\e[38;2;235;188;1m" TOP_BLOCK "\e[48;2;94;75;1m\e[38;2;255;204;1m" TOP_BLOCK "\e[48;2;160;128;1m" TOP_BLOCK "\e[48;2;194;155;1m" TOP_BLOCK "\e[48;2;198;158;1m" TOP_BLOCK "\e[48;2;174;139;1m" TOP_BLOCK "\e[48;2;122;98;1m" TOP_BLOCK "\e[48;2;40;32;1m\e[38;2;253;203;1m" TOP_BLOCK "\e[0m\e[38;2;157;126;1m" TOP_BLOCK "\e[38;2;21;17;1m" TOP_BLOCK "          \e[38;2;19;15;1m" TOP_BLOCK "\e[38;2;126;101;1m" TOP_BLOCK "\e[48;2;12;10;1m\e[38;2;153;124;1m" TOP_BLOCK "\e[48;2;39;32;1m\e[38;2;156;126;1m" TOP_BLOCK TOP_BLOCK "\e[48;2;14;12;1m\e[38;2;155;125;1m" TOP_BLOCK "\e[0m\e[38;2;115;93;1m" TOP_BLOCK "\e[38;2;31;25;1m" TOP_BLOCK "     \e[0m\n",
    };

    for (size_t i = 0; i < n_lines; ++i) {
      if (i >= padding_colour_h && i < padding_colour_h + splash_colour_h)
        printf("%*c%s", (unsigned)padding_colour_w, ' ', splash_colour[i - padding_colour_h]);
      else
        printf("\n");
    }
  }
}
